
Scenario: Customer is allowed to apply upto 3 Gift card towards their order.
Meta:
@c2
@acceptance UGPW-4858
Given aeSite header set as <aeSite>
And user type <userType>
When I add 'commerce' item 'without promotion' with quantity 9
Then there are no errors and status is success
When I add shipping address to cart: commerce/data/common/shippingAddress/<shippingAddress>
Then there are no errors and status is success
When I add gift card 'Gift_Card_75_USD_01'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_02'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_03'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_01'
Then there are the following errors in response:
